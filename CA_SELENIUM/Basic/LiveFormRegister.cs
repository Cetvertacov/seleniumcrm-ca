﻿using CA_SELENIUM.BaseModule;
using CA_SELENIUM.Models.Forms;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace CA_SELENIUM.Basic
{
    public class LiveFormRegister
    {
        IWebDriver driver;
        private const string BASE_URL = BaseTest.BASE_URL_CA;
        private const string LOCAL_PATH_TO_DRIVER = BaseTest.LOCAL_PATH_TO_DRIVER;

        private const string URL_PATH_TO_GET_1 = @"forms/liveaccount/";
        private const string URL_PATH_TO_GET_2 = @"CrmUser/AllClients";

        private const string FIRST_NAME = "FirstName";
        private const string LAST_NAME = "LastName";
        private const string TEST_EMAIL = "@yopmail.com";

        public delegate string RandomString(int lenght,string concat);
        RandomString randomString = BaseTest.randomString;


        [SetUp]
        public void InitializeChrome()
        {
            driver = new ChromeDriver(LOCAL_PATH_TO_DRIVER);
            driver.Manage().Window.Maximize();
        }

        [Test]
        public void register_live_form_from_ca()
        {
            driver.Url = BASE_URL + URL_PATH_TO_GET_1;
            string firstNameInsert = randomString(4, FIRST_NAME);
            string lastNameInsert = randomString(4, LAST_NAME);
            string emailInsert = lastNameInsert + TEST_EMAIL;           

            var liveForm = new LiveFormModel(driver);
            liveForm.TitleDdlOptions.SelectByIndex(1);
            liveForm.FirstName.SendKeys(firstNameInsert);
            liveForm.LastName.SendKeys(lastNameInsert);
            liveForm.Email.SendKeys(emailInsert);
            liveForm.TradingPlatformDdlOptions.SelectByValue("MT4");
            liveForm.AccountTypeDdlOptions.SelectByIndex(1);
            liveForm.LeverageDdlOptions.SelectByIndex(1);

            if (!liveForm.TermsAndConditionsCheckBox.Selected)
            {
                liveForm.TermsAndConditionsCheckBox.Click();
            }
            liveForm.Captcha.SendKeys("TEST");
            liveForm.SubmitBtn.Click();

            //TODO in json to specify FORM type: dynamic/static
            BaseTest.waitElemntToDisapearById(driver, "thankYouBox", 60);
        }


        [TearDown]
        public void EndTest()
        {
           
            driver.Close();
            BaseTest.killChromeProcess();
        }

    }
}
