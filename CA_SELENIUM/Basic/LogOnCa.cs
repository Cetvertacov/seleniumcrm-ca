﻿using CA_SELENIUM.BaseModule;
using CA_SELENIUM.Models.LogOn;
using log4net;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.Threading;

namespace CA_SELENIUM.Basic
{
    public class LogOnCa
    {
        IWebDriver driver;
        private const string LOCAL_PATH_TO_DRIVER = BaseTest.LOCAL_PATH_TO_DRIVER;
        private const string BASE_URL = BaseTest.BASE_URL_CA;

        private const string URL_PATH_TO_GET_1 = "Account/LogOn";

        private LogOnModel logOnModel;

        public readonly ILog logger = LogManager.GetLogger(typeof(LogOnCa));

        public LogOnCa()
        { }

        public LogOnCa(IWebDriver driver)
        {
            this.driver = driver;
        }

        [SetUp]
        public void InitializeChrome()
        {
            driver = new ChromeDriver(LOCAL_PATH_TO_DRIVER);
            
            driver.Manage().Window.Maximize();
        }

        [Test]
        public void log_on_ca()
        {
          //  driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(10);
            driver.Url = BASE_URL + URL_PATH_TO_GET_1;
            

            //driver.FindElement(By.XPath(ConstantsToUse.XPATH_TO_EN)).Click();

            logOnModel = new LogOnModel(driver);
            logOnModel.Email.SendKeys(BaseTest.ADMIN_EMAIL);
            logOnModel.Password.SendKeys(BaseTest.ADMIN_PASS);
            logOnModel.LoginBtn.Click();

            //Thread.Sleep(1000);

           
        }


        [TearDown]
        public void EndTest()
        {
            Thread.Sleep(2000);
            driver.Close();
            BaseTest.killChromeProcess();


        }
    }
}
