﻿using CA_SELENIUM.BaseModule;
using CA_SELENIUM.Models.Forms;
using CA_SELENIUM.Models.Forms.Demo;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace CA_SELENIUM.Basic
{
    public class DemoFormRegister
    {
        IWebDriver driver;
        private const string BASE_URL = BaseTest.BASE_URL_CA;
        private const string LOCAL_PATH_TO_DRIVER = BaseTest.LOCAL_PATH_TO_DRIVER;

        private const string URL_PATH_TO_GET_1 = @"forms/demoaccount/";       

        private const string FIRST_NAME = "FirstName";
        private const string LAST_NAME = "LastName";
        private const string TEST_EMAIL = "@yopmail.com";

        public delegate string RandomString(int lenght,string concat);
        RandomString randomString = BaseTest.randomString;

        DemoFormModel demoForm { get; set; }
        DemoFormSecondStepModel demoFormSecondStep { get; set; }


        [SetUp]
        public void InitializeChrome()
        {
            driver = new ChromeDriver(LOCAL_PATH_TO_DRIVER);
            driver.Manage().Window.Maximize();
        }

        [Test]
        public void register_demo_form_from_ca()
        {
            driver.Url = BASE_URL + URL_PATH_TO_GET_1;
            string firstNameInsert = randomString(4, FIRST_NAME);
            string lastNameInsert = randomString(4, LAST_NAME);
            string emailInsert = lastNameInsert + TEST_EMAIL;           

            var demoForm = new DemoFormModel(driver);

            demoForm.FirstName.SendKeys(firstNameInsert);
            demoForm.LastName.SendKeys(lastNameInsert);
            demoForm.Email.SendKeys(emailInsert);
            demoForm.CountryDdlOptions.SelectByIndex(1);
            demoForm.ContinueBtn.Click();

            demoFormSecondStep = new DemoFormSecondStepModel(driver);
            demoFormSecondStep.TradingPlatformDdlOptions.SelectByValue("MT4");
            demoFormSecondStep.AccountTypeDdlOptions.SelectByIndex(1);
            demoFormSecondStep.LeverageDdlOptions.SelectByIndex(1);
            demoFormSecondStep.AccountBaseCurrencyDdlOptions.SelectByIndex(1);

            demoFormSecondStep.Submitbtn.Click();
            //TODO in json to specify FORM type: dynamic/static
             BaseTest.waitElemntToDisapearById(driver, "thankYouBox", 60);
        }


        [TearDown]
        public void EndTest()
        {
           
            driver.Close();
            BaseTest.killChromeProcess();
        }

    }
}
