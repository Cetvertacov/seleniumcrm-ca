﻿using CA_SELENIUM.BaseModule;
using CA_SELENIUM.Enums;
using CA_SELENIUM.Models.Deposit;
using CA_SELENIUM.Models.GeneralUI;
using log4net;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace CA_SELENIUM.Basic
{
    public class MakeDeposit
    {
        IWebDriver driver;
        private const string LOCAL_PATH_TO_DRIVER = BaseTest.LOCAL_PATH_TO_DRIVER;

        private SlideMenuModel slideMenuModel;
        private DepositFundsModel depositFundsModel;
        private CCDCsubPaymentsModel subPaymentCCDC;

        //public readonly ILog logger = LogManager.GetLogger(typeof(LogOnCa));

        private List<string> pmList = new SubPaymentsList().SubPaymentsEnum;

        public MakeDeposit()
        { }

        public MakeDeposit(IWebDriver driver)
        {
            this.driver = driver;
        }

        [SetUp]
        public void InitializeChrome()
        {
            driver = new ChromeDriver(LOCAL_PATH_TO_DRIVER);
            
            driver.Manage().Window.Maximize();
        }

        [Test]
        public void make_deposit()
        {
            var logOn = new LogOnCa(driver);
            logOn.log_on_ca();

            slideMenuModel = new SlideMenuModel(driver);

            slideMenuModel.DepositAndWithdrawals.Click();
            slideMenuModel.DepositAndWithdrawalsDdl.DepositFunds.Click();

            depositFundsModel = new DepositFundsModel(driver);
            if (depositFundsModel.TradingAccountDdlOptions.Options.Count < 2)
            {
                depositFundsModel.TradingAccountDdlOptions.SelectByIndex(1);
            }
            depositFundsModel.PaymentMethodsRdBoxes.FirstOrDefault(x => x.GetAttribute("value") == "CC_DC").Click(); //TODO not only for CC DC

            subPaymentCCDC = new CCDCsubPaymentsModel(driver);

            if (subPaymentCCDC.AmountTxBox.fieldExist)
            {
                subPaymentCCDC.AmountTxBox.AmountTxBox.SendKeys("1000");
            }
            if (subPaymentCCDC.BankDdll.fieldExist)
            {
                subPaymentCCDC.BankDdll.BankDdlOptions.SelectByIndex(1);
            }
            foreach (var pm in pmList)
            { }
             

           // depositFundsModel.initSubPaymentMethodDdlOptions(driver, depositFundsModel);

            //depositFundsModel.SubPaymentsMethodOptions.SelectByValue("BPOTC");






        }


        [TearDown]
        public void EndTest()
        {
            Thread.Sleep(2000);
            driver.Close();
            BaseTest.killChromeProcess();


        }
    }
}
