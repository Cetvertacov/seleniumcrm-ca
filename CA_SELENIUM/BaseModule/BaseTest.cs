﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Diagnostics;
using System.Linq;

namespace CA_SELENIUM.BaseModule
{
    public static class BaseTest
    {
        public const string LOCAL_PATH_TO_DRIVER = @"D:\dev\QAfolder\";

        public const string XPATH_TO_EN = "/html/body/div[1]/div/div[2]/div/a[1]/img";

        public const string BASE_URL_CA = @"http://test.crm4you.pro:5774/en/";

        public const string ADMIN_EMAIL = "it@primefin.tech";
        public const string ADMIN_PASS = "Test123!";
        private static Random random = new Random();

        public static void killChromeProcess()
        {
            var processes = Process.GetProcessesByName("chromedriver");
            foreach (var temp in processes)
            {
                temp.Kill();
            }
        }

        public static string randomString(int length, string concat)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var temp = new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());

            return temp + concat;
        }


        public static void waitElemntVisibleByClassname(IWebDriver driver, string className, int seconds)
        {
            TimeSpan timeWait = TimeSpan.FromSeconds(seconds);
            WebDriverWait wait = new WebDriverWait(driver, timeWait);
            wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(By.ClassName(className)));
        }

        public static void waitElemntVisibleById(IWebDriver driver, string Id, int seconds)
        {
            TimeSpan timeWait = TimeSpan.FromSeconds(seconds);
            WebDriverWait wait = new WebDriverWait(driver, timeWait);
            wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(By.Id(Id)));
        }

        public static void waitElemntToDisapearById(IWebDriver driver, string Id, int seconds)
        {
            TimeSpan timeWait = TimeSpan.FromSeconds(seconds);
            WebDriverWait wait = new WebDriverWait(driver, timeWait);
            wait.Until(ExpectedConditions.InvisibilityOfElementLocated(By.Id(Id)));
        }

        //  public static void initialize(IWebDriver driver)

        public static void waitElemntToDisapearByXPath(IWebDriver driver, string ByXPath, int seconds)
        {
            TimeSpan timeWait = TimeSpan.FromSeconds(seconds);
            WebDriverWait wait = new WebDriverWait(driver, timeWait);
            wait.Until(ExpectedConditions.InvisibilityOfElementLocated(By.XPath(ByXPath)));
        }

        public static void waitElemntVisibleByXPath(IWebDriver driver, string ByXPath, int seconds)
        {
            TimeSpan timeWait = TimeSpan.FromSeconds(seconds);
            WebDriverWait wait = new WebDriverWait(driver, timeWait);
            wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(By.XPath(ByXPath)));
        }

        public static void waitElemntToBeClickableByXPath(IWebDriver driver, string ByXPath, int seconds)
        {
            TimeSpan timeWait = TimeSpan.FromSeconds(seconds);
            WebDriverWait wait = new WebDriverWait(driver, timeWait);
            wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath(ByXPath)));
        }

        public static void waitElemntToBeClickableById(IWebDriver driver, string Id, int seconds)
        {
            TimeSpan timeWait = TimeSpan.FromSeconds(seconds);
            WebDriverWait wait = new WebDriverWait(driver, timeWait);
            wait.Until(ExpectedConditions.ElementToBeClickable(By.Id(Id)));
        }

        public static void waitElemntToBeClickableByClassName(IWebDriver driver, string className, int seconds)
        {
            TimeSpan timeWait = TimeSpan.FromSeconds(seconds);
            WebDriverWait wait = new WebDriverWait(driver, timeWait);
            wait.Until(ExpectedConditions.ElementToBeClickable(By.ClassName(className)));
        }

        public static void waitElementExistsByClassName(IWebDriver driver, string className, int seconds)
        {
            TimeSpan timeWait = TimeSpan.FromSeconds(seconds);
            WebDriverWait wait = new WebDriverWait(driver, timeWait);
            wait.Until(ExpectedConditions.ElementExists(By.ClassName(className)));
        }
    }


 

}
