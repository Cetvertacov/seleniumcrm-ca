﻿using CA_SELENIUM.BaseModule;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace CA_SELENIUM.Models.Forms.Demo
{
    public class DemoFormSecondStepModel
    {
        private readonly string TradingPlatformDdlSelector= "TradingSystemKey";
        private readonly string AccountTypeDdlSelector = "AccountTypeKey";
        private readonly string LeverageDdlSelector = "LeverageKey";
        private readonly string AccountBaseCurrencyDdlSelector = "AccountBaseCurrencyKey";
        private readonly string SubmitbtnSelector = "//*[@id='submit_form']/div/div[2]/div/div/input[1]";


        public IWebElement TradingPlatformDdl { get; set; }
        public IWebElement AccountTypeDdl { get; set; }
        public IWebElement LeverageDdl { get; set; }
        public IWebElement AccountBaseCurrencyDdl { get; set; }
        public IWebElement Submitbtn { get; set; }


        public SelectElement TradingPlatformDdlOptions { get; set; }
        public SelectElement AccountTypeDdlOptions { get; set; }
        public SelectElement LeverageDdlOptions { get; set; }
        public SelectElement AccountBaseCurrencyDdlOptions { get; set; }


        public DemoFormSecondStepModel(IWebDriver driver)
        {
            BaseTest.waitElemntVisibleById(driver, TradingPlatformDdlSelector, 8);

            TradingPlatformDdl = driver.FindElement(By.Id(TradingPlatformDdlSelector));
            AccountTypeDdl = driver.FindElement(By.Id(AccountTypeDdlSelector));
            LeverageDdl = driver.FindElement(By.Id(LeverageDdlSelector));
            AccountBaseCurrencyDdl = driver.FindElement(By.Id(AccountBaseCurrencyDdlSelector));

            TradingPlatformDdlOptions = new SelectElement(TradingPlatformDdl);
            AccountTypeDdlOptions = new SelectElement(AccountTypeDdl);
            LeverageDdlOptions = new SelectElement(LeverageDdl);
            AccountBaseCurrencyDdlOptions = new SelectElement(AccountBaseCurrencyDdl);

            Submitbtn = driver.FindElement(By.XPath(SubmitbtnSelector));

        }
    }
}
