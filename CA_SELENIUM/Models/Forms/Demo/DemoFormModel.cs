﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace CA_SELENIUM.Models.Forms.Demo
{
    public class DemoFormModel
    {
        private readonly string TitleDdlSelector = "TitleKey";
        private readonly string FirstNameSelector = "Firstname";
        private readonly string LastNameSelector = "Lastname";
        private readonly string EmailSelector = "Email";
        private readonly string EmailConfirmationSelector = "ConfirmEmail";
        private readonly string CountryDdlSelector = "CountryKey";
        private readonly string NationalityDdlSelector = "NationalityKey";
        private readonly string PhoneSelector = "Phone";
        private readonly string QQSelector = "QQ";
        private readonly string WeChatSelector = "WeChat";
        private readonly string AddressSelector = "Address";
        private readonly string CitySelector = "City";
        private readonly string ZipPostalCodeSelector = "ZipCode";
        private readonly string ContinueBtnSelector = "formContinueButton";


        public IWebElement TitleDdl { get; set; }
        public IWebElement FirstName { get; set; }
        public IWebElement LastName { get; set; }
        public IWebElement Email { get; set; }
        public IWebElement EmailConfirmation { get; set; }
        public IWebElement CountryDdl { get; set; }
        public IWebElement NationalityDdl { get; set; }
        public IWebElement Phone { get; set; }
        public IWebElement QQ { get; set; }
        public IWebElement WeChat { get; set; }
        public IWebElement Address { get; set; }
        public IWebElement City { get; set; }
        public IWebElement ZipPostalCode { get; set; }
        public IWebElement ContinueBtn { get; set; }
        
        public SelectElement TitleDdlOptions { get; set; }
        public SelectElement TradingPlatformDdlOptions { get; set; }
        public SelectElement AccountTypeDdlOptions { get; set; }
        public SelectElement LeverageDdlOptions { get; set; }
        public SelectElement CountryDdlOptions { get; set; }
        public SelectElement NationalityDdlOptions { get; set; }



        public DemoFormModel(IWebDriver driver)
        {
            FirstName = driver.FindElement(By.Id(FirstNameSelector));
            LastName = driver.FindElement(By.Id(LastNameSelector));
            Email = driver.FindElement(By.Id(EmailSelector));
            CountryDdl = driver.FindElement(By.Id(CountryDdlSelector));
           
            CountryDdlOptions = new SelectElement(CountryDdl);

            ContinueBtn = driver.FindElement(By.Id(ContinueBtnSelector));
        }
    }
}
