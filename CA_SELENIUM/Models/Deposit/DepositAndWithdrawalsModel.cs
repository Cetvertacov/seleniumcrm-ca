﻿using OpenQA.Selenium;

namespace CA_SELENIUM.Models.Deposit
{
    public class DepositAndWithdrawalsModel
    {
        public IWebElement DepositFunds { get; set; }
        private string DepositFundsSelector = "sidebar-submenu-fund-deposit";

        public IWebElement WithdrawFunds { get; set; }
        private string WithdrawFundsSelector = "sidebar-submenu-fund-withdrawal";


        public DepositAndWithdrawalsModel(IWebDriver driver)
        {
            DepositFunds = driver.FindElement(By.Id(DepositFundsSelector));
            WithdrawFunds = driver.FindElement(By.Id(WithdrawFundsSelector));
        }
    }

}
