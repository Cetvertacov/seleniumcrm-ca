﻿using OpenQA.Selenium;

namespace CA_SELENIUM.Models.Deposit.CC_DC
{
    public class AmountTxBxModel
    {
        private string AmountTxBoxSelector = "amount-div";
        public bool fieldExist;

        public IWebElement AmountTxBox { get; set; }

        public AmountTxBxModel(IWebDriver driver)
        {
            try
            {
                AmountTxBox = driver.FindElement(By.Id(AmountTxBoxSelector));
                fieldExist = true;
            }

            catch (NoSuchElementException)
            {
                fieldExist = false;
            }
        }
    }
}
