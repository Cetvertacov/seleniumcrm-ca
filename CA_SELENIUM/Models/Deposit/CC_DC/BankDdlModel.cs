﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_SELENIUM.Models.Deposit.CC_DC
{
    public class BankDdlModel
    {
        private string BankDdlSelector = "BankName";
        public bool fieldExist;

        private IWebElement BankDdl { get; set; }
        public SelectElement BankDdlOptions { get; set; }
        public BankDdlModel(IWebDriver driver)
        {
            try
            {
                BankDdl = driver.FindElement(By.Id(BankDdlSelector));
                BankDdlOptions = new SelectElement(BankDdl);
                fieldExist = true;
            }
            catch (NoSuchElementException)
            {
                fieldExist = false;
            }
        }
    }
}
