﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_SELENIUM.Models.Deposit.CC_DC
{
    public class CurrencyDdlModel
    {
        private string CurrencyDdlSelector = "Currency";
        public bool fieldExist;

        private IWebElement CurrencyDdl { get; set; }
        public SelectElement CurrencyDdlOptions { get; set; }
        public CurrencyDdlModel(IWebDriver driver)
        {
            try
            {
                CurrencyDdl = driver.FindElement(By.Id(CurrencyDdlSelector));
                CurrencyDdlOptions = new SelectElement(CurrencyDdl);
                fieldExist = true;
            }
            catch (NoSuchElementException)
            {
                fieldExist = false;
            }
        }
    }
}
