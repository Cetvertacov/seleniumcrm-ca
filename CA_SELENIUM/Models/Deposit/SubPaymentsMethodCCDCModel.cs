﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace CA_SELENIUM.Models.Deposit
{
    public class SubPaymentsMethodCCDCModel
    {
        private string SubPaymentsDdlSelector = "PaymentMethod";
        private string CurrencyDdlSelector = "Currency";
        private string AmountTxBoxSelector = "amount-div";
        private string BankDdlSelector = "BankName";

        private IWebElement SubPaymentsDdl { get; set; }
        private IWebElement CurrencyDdl { get; set; }
        public IWebElement AmountTxBox { get; set; }
        private IWebElement BankDdl { get; set; }

        public SelectElement SubPaymentsMethodOptions { get; set; }
        public SelectElement BankDdlOptions { get; set; }
        public SubPaymentsMethodCCDCModel(IWebDriver driver)
        {
            SubPaymentsDdl = driver.FindElement(By.Id(SubPaymentsDdlSelector));
            SubPaymentsMethodOptions = new SelectElement(SubPaymentsDdl);
        }

        public void initAmountTextBox(IWebDriver driver, SubPaymentsMethodCCDCModel objekt)
        {
            AmountTxBox = driver.FindElement(By.Id(AmountTxBoxSelector));
        }
        public void initBankDdlOptions(IWebDriver driver, SubPaymentsMethodCCDCModel objekt)
        {
            BankDdl = driver.FindElement(By.Id(BankDdlSelector));
            BankDdlOptions = new SelectElement(BankDdl);
        }
        public void initCurrencyDdlOptions(IWebDriver driver, SubPaymentsMethodCCDCModel objekt)
        {
            BankDdl = driver.FindElement(By.Id(BankDdlSelector));
            BankDdlOptions = new SelectElement(BankDdl);
        }
    }
}
