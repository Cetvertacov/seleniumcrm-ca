﻿using OpenQA.Selenium;

namespace CA_SELENIUM.Models.AffiliateProgram
{
    public class AffiliateProgramModel
    {
        public IWebElement ApplyForIB { get; set; }

        public AffiliateProgramModel(IWebDriver driver)
        {
            ApplyForIB = driver.FindElement(By.Id("sidebar-submenu-affiliateProgram-applyForIb"));
        }

    }
}
