﻿using OpenQA.Selenium;

namespace CA_SELENIUM.Models.AffiliateProgram
{
    public class ApplyForIBFormModel
    {
        public IWebElement TermsAndCondCkBox { get; set; }
        public IWebElement SubmitBtn { get; set; }

        public ApplyForIBFormModel(IWebDriver driver)
        {
            TermsAndCondCkBox = driver.FindElement(By.Id("HasAcceptedTermsAndConditions"));
            SubmitBtn = driver.FindElement(By.Id("submit"));
        }
    }
}
