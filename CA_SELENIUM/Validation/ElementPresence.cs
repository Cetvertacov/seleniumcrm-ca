﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_SELENIUM.Validation
{
   public class ElementPresence
    {
        private IWebDriver driver;
        public ElementPresence(IWebDriver driver)
        {
            driver = this.driver;
        }


        public bool IsElementPresent(By by)
        {
            try
            {
                driver.FindElement(by);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }
    }
    
}
