﻿using log4net;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Xml;

namespace CRM_SELENIUM.BaseModule
{
    public static class BaseTest
    {
        public const string XPATH_TO_EN = "/html/body/div[1]/div/div[2]/div/a[1]/img";

        public const string LOCAL_PATH_TO_DRIVER = @"D:\dev\QAfolder\";

        public const string BASE_URL_CRM = @"http://test.crm4you.pro:5775/en/";

        public const string ADMIN_EMAIL = "it@primefin.tech";
        public const string ADMIN_PASS = "Test123!";
        private static Random random = new Random();

        public static void killChromeProcess()
        {
            var processes = Process.GetProcessesByName("chromedriver");
            foreach (var temp in processes)
            {
                temp.Kill();
            }
        }

        public static void WaitSpinnerDisapear(IWebDriver driver)
        {
            var waitWhileSpinner = driver.FindElement(By.Id("loading")).GetCssValue("display");
            while (waitWhileSpinner != "none")
            {
                Thread.Sleep(1000);
                waitWhileSpinner = driver.FindElement(By.Id("loading")).GetCssValue("display");
            }
        }
        public static void initLogger()
        {
            XmlDocument log4netConfig = new XmlDocument();
            var temp = File.OpenRead(@"D:\dev\seleniumcrm-ca\CRM_SELENIUM\log4net.config");
            log4netConfig.Load(temp);

            var repo = LogManager.CreateRepository(Assembly.GetCallingAssembly(), typeof(log4net.Repository.Hierarchy.Hierarchy));
            log4net.Config.XmlConfigurator.Configure(repo, log4netConfig["log4net"]);
        }

        public static string randomString(int length, string concat)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var temp = new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());

            return concat + temp;
        }

        public static string randomNumbers(int length)
        {
            const string chars = "123456789";
            var temp = new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());

            return temp;
        }

        public static void waitPresenceOfElementByXpath(IWebDriver driver, string xPath, int seconds)
        {
            TimeSpan timeWait = TimeSpan.FromSeconds(seconds);
            WebDriverWait wait = new WebDriverWait(driver, timeWait);
            wait.Until(ExpectedConditions.PresenceOfAllElementsLocatedBy(By.XPath(xPath)));
        }

        public static void waitElemntVisibleById(IWebDriver driver, string Id, int seconds)
        {
            TimeSpan timeWait = TimeSpan.FromSeconds(seconds);
            WebDriverWait wait = new WebDriverWait(driver, timeWait);
            wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(By.Id(Id)));
        }

        public static void waitElemntVisibleByClassName(IWebDriver driver, string className, int seconds)
        {
            TimeSpan timeWait = TimeSpan.FromSeconds(seconds);
            WebDriverWait wait = new WebDriverWait(driver, timeWait);
            wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(By.ClassName(className)));
        }

        public static void waitElemntToDisapearById(IWebDriver driver, string Id, int seconds)
        {
            TimeSpan timeWait = TimeSpan.FromSeconds(seconds);
            WebDriverWait wait = new WebDriverWait(driver, timeWait);
            wait.Until(ExpectedConditions.InvisibilityOfElementLocated(By.Id(Id)));
        }

        public static void waitElemntToDisapearByClassName(IWebDriver driver, string className, int seconds)
        {
            TimeSpan timeWait = TimeSpan.FromSeconds(seconds);
            WebDriverWait wait = new WebDriverWait(driver, timeWait);
            wait.Until(ExpectedConditions.InvisibilityOfElementLocated(By.ClassName(className)));
        }

        public static void waitElemntToDisapearByXPath(IWebDriver driver, string ByXPath, int seconds)
        {
            TimeSpan timeWait = TimeSpan.FromSeconds(seconds);
            WebDriverWait wait = new WebDriverWait(driver, timeWait);
            wait.Until(ExpectedConditions.InvisibilityOfElementLocated(By.XPath(ByXPath)));
        }

        public static void waitElemntVisibleByXPath(IWebDriver driver, string ByXPath, int seconds)
        {
            TimeSpan timeWait = TimeSpan.FromSeconds(seconds);
            WebDriverWait wait = new WebDriverWait(driver, timeWait);
            wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(By.XPath(ByXPath)));
        }

        public static void waitElemntVisibleCssSelector(IWebDriver driver, string cssSelector, int seconds)
        {
            TimeSpan timeWait = TimeSpan.FromSeconds(seconds);
            WebDriverWait wait = new WebDriverWait(driver, timeWait);
            wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(By.CssSelector(cssSelector)));
        }

        public static void waitElemntToBeClickableByXPath(IWebDriver driver, string ByXPath, int seconds)
        {
            TimeSpan timeWait = TimeSpan.FromSeconds(seconds);
            WebDriverWait wait = new WebDriverWait(driver, timeWait);
            wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath(ByXPath)));
        }

        public static void waitElemntToBeClickableById(IWebDriver driver, string Id, int seconds)
        {
            TimeSpan timeWait = TimeSpan.FromSeconds(seconds);
            WebDriverWait wait = new WebDriverWait(driver, timeWait);
            wait.Until(ExpectedConditions.ElementToBeClickable(By.Id(Id)));
        }

        public static void WaitElemntToBeClickableByCssSelector(IWebDriver driver, string cssSelector, int seconds)
        {
            TimeSpan timeWait = TimeSpan.FromSeconds(seconds);
            WebDriverWait wait = new WebDriverWait(driver, timeWait);
            wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector(cssSelector)));
        }

        public static void WaitVisibilityOfElementsLocatedBy(IWebDriver driver, ReadOnlyCollection<IWebElement> collection, int seconds)
        {
            TimeSpan timeWait = TimeSpan.FromSeconds(seconds);
            WebDriverWait wait = new WebDriverWait(driver, timeWait);
            wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(collection));
        }





        public static void TryGetElementByCssSelector(IWebDriver driver, string cssSelector, int seconds)
        {
           // TimeSpan timeWait = TimeSpan.FromSeconds(seconds);

            try
            {
                if (IsTimerToWaitSeconds(seconds))
                {
                    driver.FindElement(By.CssSelector(cssSelector));
                }
            }
            catch {
                if (!IsTimerToWaitSeconds(seconds))
                {
                    throw new Exception("");
                }
                Thread.Sleep(2000);
                TryGetElementByCssSelector(driver, cssSelector, seconds);
            }
        }

        public static bool IsTimerToWaitSeconds(int seconds)
        {
            Stopwatch timer = Stopwatch.StartNew();
            if (timer.ElapsedMilliseconds / 1000 < seconds)
                return true;
            else
                return false;
        }
    }




}
