﻿using log4net;
using System.IO;
using System.Reflection;
using System.Xml;

namespace CRM_SELENIUM
{
    public class Log4NetConfig
    {
        public Log4NetConfig()
        {
            XmlDocument log4netConfig = new XmlDocument();
            var temp = File.OpenRead(@"D:\dev\seleniumcrm-ca\CRM_SELENIUM\log4net.config");
            log4netConfig.Load(temp);

            var repo = LogManager.CreateRepository(Assembly.GetCallingAssembly(), typeof(log4net.Repository.Hierarchy.Hierarchy));
            log4net.Config.XmlConfigurator.Configure(repo, log4netConfig["log4net"]);
        }
    }
}
