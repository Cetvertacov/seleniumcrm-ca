﻿using CRM_SELENIUM.BaseModule;
using CRM_SELENIUM.Models.MenuModels;
using OpenQA.Selenium;

namespace CRM_SELENIUM.Models
{
    public class SlideMenuModel
    {
        private readonly string DashboardSelector = "dashboard-menu-item";
        private readonly string BackOfficeSelector = "backoffice-menu-item";
        private readonly string CRMSelector = "crm-menu-item";
        private readonly string CommunicationSelector = "crm-communication-item";
        private readonly string ReportsSelector = "crm-reports-item";
        private readonly string ConfigurationsSelector = "crm-configarations-item";
        private readonly string EmployeeSelector = "crm-employee-item";


        public IWebElement Dashboard { get; set; }
        public IWebElement BackOffice { get; set; }
        public IWebElement Crm { get; set; }
        public IWebElement Communication { get; set; }
        public IWebElement Reports { get; set; }
        public IWebElement Configurations { get; set; }
        public IWebElement Employee { get; set; }

        public SlideMenuCRMModel CrmDdlOptions { get; set; }
        public SubMenuConfigDdlModel ConfigurationsDdlOptions { get; set; }
        public SlideMenuEmployeeModel EmployeeDdlOptions { get; set; }
        public SlideMenuModel(IWebDriver driver)
        {
            BaseTest.waitElemntVisibleById(driver, DashboardSelector, 5);

            Dashboard = driver.FindElement(By.Id(DashboardSelector));
            BackOffice = driver.FindElement(By.Id(BackOfficeSelector));
            Crm = driver.FindElement(By.Id(CRMSelector));
            Communication = driver.FindElement(By.Id(CommunicationSelector));
            Reports = driver.FindElement(By.Id(ReportsSelector));
            Configurations = driver.FindElement(By.Id(ConfigurationsSelector));
            Employee = driver.FindElement(By.Id(EmployeeSelector));

            //CrmDdl = new SlideMenuCRMModel(driver);
           // ConfigurationsDdl = new SubMenuConfigDdlModel(driver);
        }
        public void initConfigurationDdl(IWebDriver driver)
        {
            ConfigurationsDdlOptions = new SubMenuConfigDdlModel(driver);

        }

        public void initEmployeeDdl(IWebDriver driver)
        {
            EmployeeDdlOptions = new SlideMenuEmployeeModel(driver);
        }

        public void initCRMDdl(IWebDriver driver)
        {
            CrmDdlOptions = new SlideMenuCRMModel(driver);
        }

    }
}
