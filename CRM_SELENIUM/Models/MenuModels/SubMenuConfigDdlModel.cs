﻿using OpenQA.Selenium;

namespace CRM_SELENIUM.Models.MenuModels
{
    public class SubMenuConfigDdlModel
    {
        private readonly string DataBaseSelector = "database-submenu-item";
        private readonly string IBCommConfigSelector = "";
        private readonly string ProductsListSelector = "";
        private readonly string GroupsListSelector = "";
        private readonly string SymbolsListSelector = "";
        private readonly string MarketingSelector = "";
        private readonly string FormConfigSelector = "";
        private readonly string ReportsConfigSelector = "";
        private readonly string CustomizedConfigSelector = "";
        private readonly string SecurityConfigSelector = "";
        private readonly string SMSConfigSelector = "";
        private readonly string ImagesSelector = "";
        private readonly string PaymentsGatewaysSelector = "";
        private readonly string TradingPlatformsSelector = "";

        public IWebElement DataBase { get; set; }
        public IWebElement IBCommConfig { get; set; }
        public IWebElement ProductsList { get; set; }
        public IWebElement GroupsList { get; set; }
        public IWebElement SymbolsList { get; set; }
        public IWebElement Marketing { get; set; }
        public IWebElement FormConfig   { get; set; }
        public IWebElement ReportsConfig { get; set; }
        public IWebElement CustomizedConfig { get; set; }
        public IWebElement SecurityConfig { get; set; }
        public IWebElement SMSConfig { get; set; }
        public IWebElement Images { get; set; }
        public IWebElement PaymentGateways { get; set; }
        public IWebElement TradingPlatforms { get; set; }


        public  SubMenuConfigDdlModel(IWebDriver driver)
        {
            DataBase = driver.FindElement(By.Id(DataBaseSelector));
        }
    }
}
