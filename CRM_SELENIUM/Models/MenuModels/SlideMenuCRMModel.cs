﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace CRM_SELENIUM.Models.MenuModels
{
   public class SlideMenuCRMModel
    {
        private readonly string TradingAccountSearchSelector = "search-account-submenu-item";
        private readonly string ClientSearchSelector = "search-client-submenu-item";
        private readonly string CreateNewClientSelector = "create-client-submenu-item";
        private readonly string CreateNewLeadSelector = "create-lead-submenu-item";
        private readonly string TicketSearchSelector = "search-ticket-item";


        public IWebElement TradingAccountSearch { get; set; }
        public IWebElement ClientSearch { get; set; }
        public IWebElement CreateNewClient { get; set; }
        public IWebElement CreateNewLead { get; set; }
        public IWebElement TicketSearch { get; set; }

        public SlideMenuCRMModel(IWebDriver driver)
        {
            TradingAccountSearch = driver.FindElement(By.Id(TradingAccountSearchSelector));
            ClientSearch = driver.FindElement(By.Id(ClientSearchSelector));
            CreateNewClient = driver.FindElement(By.Id(CreateNewClientSelector));
            CreateNewLead = driver.FindElement(By.Id(CreateNewLeadSelector));
            TicketSearch = driver.FindElement(By.Id(TicketSearchSelector));
        }

    }
}
