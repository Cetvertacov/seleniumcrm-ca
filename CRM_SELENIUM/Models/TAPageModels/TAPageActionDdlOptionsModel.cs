﻿using OpenQA.Selenium;

namespace CRM_SELENIUM.Models.TAPageModels
{
    public class TAPageActionDdlOptionsModel
    {
        private readonly string InternalTransferSelector = "//a[contains(@data-target, '#internTransferPartial')]";
        private readonly string PaymentOperationsDdlSelector2 = "/html/body/div[3]/div[2]/div/div[1]/div[6]/div[1]/div[1]/div[1]/div/ul/li[2]/a"; //TODO sdelati normalino tak skazati po liudsky
        private readonly string PaymentOperationsDdlSelector1 = "/html/body/div[3]/div[2]/div/div[1]/div[6]/div[1]/div[1]/div[1]/div/ul/li[1]/a"; //in case InternalTransfer is not present on page
        private readonly string UnassignTradingAccountSelector = "//a[contains(@data-target, '#unasignedTA')]";

        public IWebElement InternalTransfer { get; set; }
        public IWebElement PaymentOperationsDdl { get; set; }
        public IWebElement UnassignTradingAccount { get; set; }

        public TAPagePayOprtDdlOptions PaymentOperationsDdlOptions { get; set; }

        public TAPageActionDdlOptionsModel(IWebDriver driver)
        {
            try //TODO if InternalTransfer not present <li> position = 1 esle = 2
            {
                InternalTransfer = driver.FindElement(By.XPath(InternalTransferSelector));
                PaymentOperationsDdl = driver.FindElement(By.XPath(PaymentOperationsDdlSelector2));
                UnassignTradingAccount = driver.FindElement(By.XPath(UnassignTradingAccountSelector));
            }
            catch
            {
                PaymentOperationsDdl = driver.FindElement(By.XPath(PaymentOperationsDdlSelector1));
                UnassignTradingAccount = driver.FindElement(By.XPath(UnassignTradingAccountSelector));
            }
        }

        public void InitPaymentOperationsDdlOptions(IWebDriver driver)
        {
            PaymentOperationsDdlOptions = new TAPagePayOprtDdlOptions(driver);
        }
        public void InitInternalTransfer(IWebDriver driver)
        {
            InternalTransfer = driver.FindElement(By.XPath(InternalTransferSelector));
        }

    }
}
