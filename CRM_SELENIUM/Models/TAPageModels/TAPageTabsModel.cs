﻿using CRM_SELENIUM.BaseModule;
using OpenQA.Selenium;

namespace CRM_SELENIUM.Models.TAPageModels
{
   public class TAPageTabsModel
    {
        private readonly string ActionDdlSelector = "//button[contains(@class, 'btn blue-oleo dropdown-toggle')]";
        public IWebElement ActionDdl { get; set; }

        public TAPageActionDdlOptionsModel ActionDdlOptions { get; set; }

        public TAPageTabsModel(IWebDriver driver)
        {
            BaseTest.waitElemntVisibleByXPath(driver, ActionDdlSelector, 8);
            ActionDdl = driver.FindElement(By.XPath(ActionDdlSelector));
        }

        public void InitTAPageActionDdlOptions(IWebDriver driver)
        {
            ActionDdlOptions = new TAPageActionDdlOptionsModel(driver);
        }
    }
}
