﻿using CRM_SELENIUM.BaseModule;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System.Collections.ObjectModel;

namespace CRM_SELENIUM.Models.Employee
{
   public class AddEmployeeDetailsModel
    {
        private readonly string DepartmentDdlSelector = "Department";
        private readonly string RoleDdlSelector = "Role";
        private readonly string SupervisorDdlSelector = "select2-Supervisor-container";
        private readonly string SubmitBtnSelector = "createUserSubmit";
        private readonly string BackBtnSelector = "//*[contains(@class, 'btn default button-previous')]";

        private readonly string SupervisorDdlOptionsSelector = "//li[contains(@class, 'select2-results__option')]";
        private readonly string FirstElementSupervisorDdlOptionsSelector = "//*[@id='select2-Supervisor-results']/li[1]";


        public IWebElement DepartmentDdl { get; set; }
        public IWebElement RoleDdl { get; set; }
        public IWebElement SupervisorDdl { get; set; } //searcheable ddl
        public IWebElement SubmitBtn { get; set; }
        public IWebElement BackBtn { get; set; }

        public SelectElement DepartmentDdlOptions { get; set; }
        public SelectElement RoleDdlOptions { get; set; }

        public ReadOnlyCollection<IWebElement> SupervisorDdlOptions { get; set; }

        public AddEmployeeDetailsModel(IWebDriver driver)
        {
            BaseTest.waitElemntVisibleById(driver, DepartmentDdlSelector, 10);

            DepartmentDdl = driver.FindElement(By.Id(DepartmentDdlSelector));
            RoleDdl = driver.FindElement(By.Id(RoleDdlSelector));
            SupervisorDdl = driver.FindElement(By.Id(SupervisorDdlSelector)); //searcheble ddl     
            SubmitBtn = driver.FindElement(By.Id(SubmitBtnSelector));
            BackBtn = driver.FindElement(By.XPath(BackBtnSelector));

            DepartmentDdlOptions = new SelectElement(DepartmentDdl);
            RoleDdlOptions = new SelectElement(RoleDdl);

        }
        public void InitSupervisorDdlOptions(IWebDriver driver)
        {
            BaseTest.waitElemntVisibleByXPath(driver, FirstElementSupervisorDdlOptionsSelector, 5);
            SupervisorDdlOptions = driver.FindElements(By.CssSelector(SupervisorDdlOptionsSelector));
        }

    }
}
