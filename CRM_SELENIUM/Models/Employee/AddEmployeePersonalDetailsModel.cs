﻿using CRM_SELENIUM.BaseModule;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace CRM_SELENIUM.Models.Employee
{
   public class AddEmployeePersonalDetailsModel
    {
        private readonly string FirstNameSelector = "FirstName";
        private readonly string LastNameSelector = "LastName";
        private readonly string CountryDdlSelctor = "CountryKey";
        private readonly string AddressSelector = "Address";
        private readonly string EmailSelector = "Email";
        private readonly string PhoneSelector = "Phone";
        private readonly string ContinueBtnSelector = "formContinueButton";

        public IWebElement FirstName { get; set; }
        public IWebElement LastName { get; set; }
        public IWebElement CountryDdl { get; set; }

        public IWebElement Address { get; set; }
        public IWebElement Email { get; set; }
        public IWebElement Phone { get; set; }
        public IWebElement ContinueBtn { get; set; }

        public SelectElement CountryDdlOptions { get; set; }

        public AddEmployeePersonalDetailsModel(IWebDriver driver)
        {
            BaseTest.waitElemntVisibleById(driver, FirstNameSelector, 10);

            FirstName = driver.FindElement(By.Id(FirstNameSelector));
            LastName = driver.FindElement(By.Id(LastNameSelector));

            CountryDdl = driver.FindElement(By.Id(CountryDdlSelctor));
            CountryDdlOptions = new SelectElement(CountryDdl);

            Address = driver.FindElement(By.Id(AddressSelector));
            Email = driver.FindElement(By.Id(EmailSelector));
            Phone = driver.FindElement(By.Id(PhoneSelector));
            ContinueBtn = driver.FindElement(By.Id(ContinueBtnSelector));

        }

    }
}
