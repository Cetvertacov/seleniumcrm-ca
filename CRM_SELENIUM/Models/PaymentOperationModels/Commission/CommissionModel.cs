﻿using CRM_SELENIUM.BaseModule;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System.Collections.ObjectModel;

namespace CRM_SELENIUM.Models.PaymentOperationModels.Commission
{
    public class CommissionModel
    {
        private readonly string TradingAccountDdlSelector = "AccountNumber";
        private readonly string AmountSelector = "Amount";
        private readonly string AssociatedClientDdlSelector = "select2-AssociatedClient-container";
        private readonly string SourceTradingAccountDdlSelector = "FromAccountNumber";
        private readonly string CommentSelector = "Comments";

        private readonly string SubmitBtnSelector = "submitBtn";

        private readonly string AssociatedClientDdlOptionsSelector = "//li[contains(@class, 'select2-results__option')]";
        // private readonly string DatePickerSelector = "";


        public IWebElement TradingAccountDdl { get; set; }
        public IWebElement Amount { get; set; }
        public IWebElement AssociatedClientDdl { get; set; }
        public IWebElement SourceTradingAccountDdl { get; set; }
        public IWebElement Comment { get; set; }
        public IWebElement SubmitBtn { get; set; }


        public SelectElement TradingAccountDdlOptions { get; set; }
        public SelectElement SourceTradingAccountDdlOptions { get; set; }

        public ReadOnlyCollection<IWebElement> AssociatedClientDdlOptions { get; set; }

        public CommissionModel(IWebDriver driver)
        {
            BaseTest.waitElemntVisibleById(driver, TradingAccountDdlSelector, 8);

            TradingAccountDdl = driver.FindElement(By.Id(TradingAccountDdlSelector));
            Amount = driver.FindElement(By.Id(AmountSelector));
            AssociatedClientDdl = driver.FindElement(By.Id(AssociatedClientDdlSelector));
            Comment = driver.FindElement(By.Id(CommentSelector));
            SubmitBtn = driver.FindElement(By.Id(SubmitBtnSelector));

            TradingAccountDdlOptions = new SelectElement(TradingAccountDdl);
        }
        public void InitAssociatedClientDdlOptions(IWebDriver driver)
        {
            AssociatedClientDdlOptions = driver.FindElements(By.XPath(AssociatedClientDdlOptionsSelector));

        }
        public void InitSourceTradingAccount(IWebDriver driver)
        {
            BaseTest.waitElemntToDisapearById(driver, "tpLoader", 5);
            BaseTest.waitElemntVisibleById(driver, SourceTradingAccountDdlSelector, 8);
            SourceTradingAccountDdl = driver.FindElement(By.Id(SourceTradingAccountDdlSelector));
            SourceTradingAccountDdlOptions = new SelectElement(SourceTradingAccountDdl);
        }
        public void InitAmount(IWebDriver driver)
        {
            Amount = driver.FindElement(By.Id(AmountSelector));
        }

    }
}
