﻿using CRM_SELENIUM.BaseModule;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace CRM_SELENIUM.Models.PaymentOperationModels.Deposit
{
    public class DepositNewFundsModel
    {
        private readonly string TradingAccountDdlSelector = "AccountNumber";
        private readonly string CurrencyDdlSelector = "Currency";
        private readonly string AmountSelector = "Amount";
        private readonly string SelectTypeDdlSelector = "DepositMethod";
        private readonly string CommentSelector = "commentArea";
        private readonly string SubmitBtnSelector = "submitDeposit";


        public IWebElement TradingAccountDdl { get; set; }
        public IWebElement CurrencyDdl { get; set; }
        public IWebElement Amount { get; set; }
        public IWebElement SelectTypeDdl { get; set; }
        public IWebElement Comment { get; set; } //is loaded after selectType is selected
        public IWebElement SubmitBtn { get; set; }

        public SelectElement TradingAccountDdlOptions { get; set; }
        public SelectElement CurrencyDdlOptions { get; set; }
        public SelectElement SelectTypeDdlOptions { get; set; }

        public DepositNewFundsModel(IWebDriver driver)
        {
            BaseTest.waitElemntVisibleById(driver, TradingAccountDdlSelector, 8);
            TradingAccountDdl = driver.FindElement(By.Id(TradingAccountDdlSelector));
            CurrencyDdl = driver.FindElement(By.Id(CurrencyDdlSelector));
            Amount = driver.FindElement(By.Id(AmountSelector));
            SelectTypeDdl = driver.FindElement(By.Id(SelectTypeDdlSelector));
            SubmitBtn = driver.FindElement(By.Id(SubmitBtnSelector));

            TradingAccountDdlOptions = new SelectElement(TradingAccountDdl);
            CurrencyDdlOptions = new SelectElement(CurrencyDdl);
            SelectTypeDdlOptions = new SelectElement(SelectTypeDdl);
        }

        public void InitCommentArea(IWebDriver driver)
        {
            BaseTest.waitElemntVisibleById(driver, CommentSelector, 8);
            Comment = driver.FindElement(By.Id(CommentSelector));
        }
    }
}
