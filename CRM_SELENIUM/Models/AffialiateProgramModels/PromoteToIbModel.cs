﻿using CRM_SELENIUM.BaseModule;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;

namespace CRM_SELENIUM.Models
{
    public class PromoteToIbModel
    {
        private readonly string TradingPlatformDllSelector = "AccountMappingPlatform";
        private readonly string TradingAccountDdlSelector = "AccountNumber";
        private readonly string BtnDefaultTAMappingAddSelector = "addCommisionAccount";

        //private readonly string ProductDllSelector = "Product";
        private readonly string ProductDllSelector = "//*[@class='btn dropdown-toggle btn-default']";
        private readonly string TypeDdlSelector = "Type";
        private readonly string TPCommProfDdlSelector = "Platform";
        private readonly string CommNamesConfigsDdlSelector = "//*[@id='select2-Config-container']";
        private readonly string CommNamesConfigsDdlOptionsSelector = "select2-results__option";
        private readonly string DisplayNameSelector = "FriendlyName";
        private readonly string BtnDCommProfMapingAddSelector = "createAssociation";

        private readonly string NewtingLevelPortletSelector = "levelField";
        private readonly string NestingLevelDdlSelector = "NestingLevel";
        private readonly string SubmitBtnSelector = "submitPromoteToIb";

        private readonly string ProductDllOptionsSelector = "//*[@id='frmPromoteToIb']/div[1]/div[4]/div/div/div[1]/div/div/div/ul/li/a";

        public IWebElement TradingPlatformDll { get; set; }
        public IWebElement TradingAccountDdl { get; set; }
        public IWebElement BtnDefaultTAMappingAdd { get; set; }
        public IWebElement BtnDCommProfAddMaping { get; set; }
        public IWebElement ProductDll { get; set; }
        public IWebElement TypeDdl { get; set; }
        public IWebElement TradingPlatformCommProfMapDdl { get; set; }
        public IWebElement CommNamesConfigsDdl { get; set; }
        public IWebElement DisplayName { get; set; }
        public IWebElement NestingLevelDdl { get; set; }
        public IWebElement SubmitBtn { get; set; }

        public List<string> TA_DDL_DEF = new List<string> { "[Select]", "Create New Trading Account" };

        public SelectElement TradingPlatformDllOptions { get; set; }
        public SelectElement TradingAccountDdlOptions { get; set; }

        public SelectElement TypeDdlOptions { get; set; }
        // public SelectElement CommNamesConfigsDdlOptions { get; set; }
        public SelectElement TradingPlatformCommProfMapDdlOptions { get; set; }
        public SelectElement NestingLevelDdlOptions { get; set; }

        public ReadOnlyCollection<IWebElement> CommNamesConfigsDdlOptions { get; set; }
        public ReadOnlyCollection<IWebElement> ProductDllOptions { get; set; }



        public PromoteToIbModel(IWebDriver driver)
        {
            BaseTest.waitElemntToBeClickableById(driver, TradingPlatformDllSelector, 8);

            TradingPlatformDll = driver.FindElement(By.Id(TradingPlatformDllSelector));
            TradingAccountDdl = driver.FindElement(By.Id(TradingAccountDdlSelector));
            BtnDefaultTAMappingAdd = driver.FindElement(By.Id(BtnDefaultTAMappingAddSelector));
            BtnDCommProfAddMaping = driver.FindElement(By.Id(BtnDCommProfMapingAddSelector));
            SubmitBtn = driver.FindElement(By.Id(SubmitBtnSelector));

            TradingPlatformDllOptions = new SelectElement(TradingPlatformDll);
            TradingAccountDdlOptions = new SelectElement(TradingAccountDdl);
        }



        public void SetCommProfileMappingFieleds(IWebDriver driver)
        {
            //BaseTest.waitElemntToBeClickableByCssSelector(driver, ProductDllSelector, 8);

            ProductDll = driver.FindElement(By.XPath(ProductDllSelector));
            DisplayName = driver.FindElement(By.Id(DisplayNameSelector));
            TypeDdl = driver.FindElement(By.Id(TypeDdlSelector));

            //CommNamesConfigsDdl = driver.FindElement(By.XPath(CommNamesConfigsDdlSelector));
            //   CommNamesConfigsDdlOptions = new SelectElement(CommNamesConfigsDdl);

            TradingPlatformCommProfMapDdl = driver.FindElement(By.Id(TPCommProfDdlSelector));

            ProductDllOptions = driver.FindElements(By.XPath(ProductDllOptionsSelector));
            TypeDdlOptions = new SelectElement(TypeDdl);
            TradingPlatformCommProfMapDdlOptions = new SelectElement(TradingPlatformCommProfMapDdl);
        }

        public void SetIbNestingLevelFields(IWebDriver driver)
        {
            string firstOptionSelector = "//*[@id='NestingLevel']/option[1]";

            BaseTest.waitElemntVisibleById(driver, NewtingLevelPortletSelector, 8);         
            NestingLevelDdl = driver.FindElement(By.Id(NestingLevelDdlSelector));
            BaseTest.waitPresenceOfElementByXpath(driver, firstOptionSelector, 8); //dynamic loaded taht's why wait for first option
            NestingLevelDdlOptions = new SelectElement(NestingLevelDdl);          
        }
        public void InitCommissionConfigDdl(IWebDriver driver)
        {
            CommNamesConfigsDdl = driver.FindElement(By.XPath(CommNamesConfigsDdlSelector));
        }
        public void InitCommissionConfigOptions(IWebDriver driver)
        {

            CommNamesConfigsDdlOptions = driver.FindElements(By.ClassName(CommNamesConfigsDdlOptionsSelector));
            if (CommNamesConfigsDdlOptions.Count < 2)
            {
                Assert.Warn("WL Does not have Fix Promo Commission config");
            }
            //List<IWebElement> temp = new List<IWebElement>(CommNamesConfigsDdlOptions);
            //temp.Remove(temp.FirstOrDefault(x => x.Text == "[Select]"));
        }

      

    }
}
