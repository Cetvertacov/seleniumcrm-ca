﻿using CRM_SELENIUM.BaseModule;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System.Collections.ObjectModel;

namespace CRM_SELENIUM.Models.SearchesModels.TradingAccountSearchModels
{
    public class TASearchExtraFilters
    {
        private readonly string CurrencyDdlSelector = "";
        private readonly string CountryDdlSelector = "";
        private readonly string CitySelector = "";
        private readonly string ZipPostalCodeSelector = "";
        private readonly string AddressSelector = "";
        private readonly string PhoneSelector = "";
        private readonly string EmailSelector = "";
        private readonly string CommentSelector = "";
        private readonly string TradingPlatformDdlSelector = "trading_platform";
        private readonly string CollapseExtendSelector = "//*[@class='tools']";

        public IWebElement CurrencyDdl { get; set; }
        public IWebElement CountryDdl { get; set; }
        public IWebElement City { get; set; }
        public IWebElement ZipPostalCode { get; set; }
        public IWebElement Address { get; set; }
        public IWebElement Phone { get; set; }
        public IWebElement Email { get; set; }
        public IWebElement Comment { get; set; }
        public IWebElement TradingPlatformDdl { get; set; }
        public IWebElement CollapseExtend { get; set; }

        public SelectElement TradingPlatformDdlOptions { get; set; }
        public SelectElement CurrencyDdlOptions { get; set; }
        public SelectElement CountryDdlOptions { get; set; }

        public TASearchExtraFilters(IWebDriver driver)
        {
            BaseTest.waitElemntToBeClickableByXPath(driver, CollapseExtendSelector, 8);

            CollapseExtend = driver.FindElement(By.XPath(CollapseExtendSelector));
            TradingPlatformDdl = driver.FindElement(By.Id(TradingPlatformDdlSelector));
            TradingPlatformDdlOptions = new SelectElement(TradingPlatformDdl);
        }






    }
}
