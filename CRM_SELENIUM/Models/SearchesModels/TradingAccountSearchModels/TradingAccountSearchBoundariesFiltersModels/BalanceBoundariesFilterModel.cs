﻿using OpenQA.Selenium;

namespace CRM_SELENIUM.Models.SearchesModels.TradingAccountSearchModels.TradingAccountSearchBoundariesFiltersModels
{
    public class BalanceBoundariesFilterModel
    {
        private readonly string BalanceFromSelector = "balance_from";
        private readonly string BalanceToSelector = "balance_to";

        public IWebElement From { get; set; }
        public IWebElement To { get; set; }

        public BalanceBoundariesFilterModel(IWebDriver driver)
        {
            From = driver.FindElement(By.Id(BalanceFromSelector));
            To = driver.FindElement(By.Id(BalanceToSelector));

        }
    }
}
