﻿using CRM_SELENIUM.BaseModule;
using CRM_SELENIUM.Models.SearchesModels.ClientSearchModels.ClientSearchBoundariesFiltersModels;
using OpenQA.Selenium;

namespace CRM_SELENIUM.Models.SearchesModels.ClientSearchModels
{
    public class ClientSearchFiltersModel
    {
        //TODO fill other fields
        private readonly string SearchBtnSelector = "ClientSearch_searchAction";
        private readonly string ResetBtnSelector = "ClientSearch_resetTableValues";

        public IWebElement SearchBtn { get; set; }

        public IWebElement ResetBtn { get; set; }

        public IBLevelFilterModel IBLevel { get; set; }

        public ClientSearchFiltersModel(IWebDriver driver)
        {
            BaseTest.waitElemntToDisapearByClassName(driver, "blockOverlay", 5);
            SearchBtn = driver.FindElement(By.Id(SearchBtnSelector));
            ResetBtn = driver.FindElement(By.Id(ResetBtnSelector));

            IBLevel = new IBLevelFilterModel(driver);


        }
    }
}
