﻿using OpenQA.Selenium;

namespace CRM_SELENIUM.Models.SearchesModels.ClientSearchModels.ClientSearchBoundariesFiltersModels
{ 
    public class IBLevelFilterModel
    {
        private readonly string IBLevelFromSelector = "IbLevel_from";
        private readonly string IBLevelToSelector = "IbLevel_to";

        public IWebElement From { get; set; }
        public IWebElement To { get; set; }

        public IBLevelFilterModel(IWebDriver driver)
        {
            From = driver.FindElement(By.Id(IBLevelFromSelector));
            To = driver.FindElement(By.Id(IBLevelToSelector));

        }
    }
}
