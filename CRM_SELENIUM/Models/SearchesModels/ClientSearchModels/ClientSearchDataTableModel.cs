﻿using CRM_SELENIUM.BaseModule;
using OpenQA.Selenium;
using System;
using System.Collections.ObjectModel;

namespace CRM_SELENIUM.Models.SearchesModels.ClientSearchModels
{
    public class ClientSearchDataTableModel
    {
        private readonly string ClientsIdsSelector = "//*[@id='ClientSearch']/tbody/tr[*]/td[1]/a";
        private readonly string IBIdsSelector =      "//*[@id='ClientSearch']/tbody/tr[*]/td[5]/a";
        public ReadOnlyCollection<IWebElement> ClientsIds { get; set; }
        public ReadOnlyCollection<IWebElement> IBIds { get; set; }

        public ClientSearchDataTableModel(IWebDriver driver)
        {
            BaseTest.waitElemntToDisapearByClassName(driver, "blockOverlay", 5);
            ClientsIds = driver.FindElements(By.XPath(ClientsIdsSelector));
            IBIds = driver.FindElements(By.XPath(IBIdsSelector));
        }
    }
}
