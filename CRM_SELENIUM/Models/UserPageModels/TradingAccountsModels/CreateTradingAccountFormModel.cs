﻿using CRM_SELENIUM.BaseModule;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.ObjectModel;

namespace CRM_SELENIUM.Models.UserPageModels.TradingAccountsModels
{
   public class CreateTradingAccountFormModel
    {
        private readonly string TradingSystemDdlSelector = "TradingSystem";
        private readonly string EnvironmentDdlSelector = "Environment";
        private readonly string TradingPlatformDdlSelector = "Platform";
        private readonly string AccountTypeDdlSelector = "AccountType";
        private readonly string AccountBaseCurrencyDdlSelector = "AccountBaseCurrency";
        private readonly string PlatformGroupDdlSelector = "select2-TradingAccountAssignGroup-container";
        private readonly string LeverageDdlSelector = "Leverage";
        private readonly string SubmitBtnSelector = "submitCreateTradingAccount";


        private readonly string PlatformGroupDdlOptionsSelector = "select2-results__option";


        public IWebElement TradingSystemDdl { get; set; }
        public IWebElement EnvironmentDdl { get; set; }
        public IWebElement TradingPlatformDdl { get; set; }
        public IWebElement AccountTypeDdl { get; set; }
        public IWebElement AccountBaseCurrencyDdl { get; set; }
        public IWebElement PlatformGroupDdl { get; set; }
        public IWebElement LeverageDdl { get; set; }
        public IWebElement SubmitBtn { get; set; }

        public SelectElement TradingSystemDdlOptions { get; set; }
        public SelectElement EnvironmentDdlOptions { get; set; }
        public SelectElement TradingPlatformDdlOptions { get; set; }
        public SelectElement AccountTypeDdlOptions { get; set; }
        public SelectElement AccountBaseCurrencyDdlOptions { get; set; }
        public ReadOnlyCollection<IWebElement> PlatformGroupDdlOptions { get; set; }
        public SelectElement LeverageDdlOptions { get; set; }



        public CreateTradingAccountFormModel(IWebDriver driver)
        {
            BaseTest.waitElemntVisibleById(driver, TradingSystemDdlSelector, 8);

            TradingSystemDdl = driver.FindElement(By.Id(TradingSystemDdlSelector));
            TradingSystemDdlOptions = new SelectElement(TradingSystemDdl);
            AccountBaseCurrencyDdl = driver.FindElement(By.Id(AccountBaseCurrencyDdlSelector));
            PlatformGroupDdl = driver.FindElement(By.Id(PlatformGroupDdlSelector));
            SubmitBtn = driver.FindElement(By.Id(SubmitBtnSelector));

            AccountBaseCurrencyDdlOptions = new SelectElement(AccountBaseCurrencyDdl);
        }

        public void InitEnvAndAccType(IWebDriver driver)
        {
            EnvironmentDdl = driver.FindElement(By.Id(EnvironmentDdlSelector));
            EnvironmentDdlOptions = TryInitDdlOptions(EnvironmentDdl, 10);
                //new SelectElement(EnvironmentDdl);

            AccountTypeDdl = driver.FindElement(By.Id(AccountTypeDdlSelector));
            AccountTypeDdlOptions = TryInitDdlOptions(AccountTypeDdl, 10);
            //new SelectElement(AccountTypeDdl);
        }

        public void InitTradingPlatform(IWebDriver driver)
        {
            TradingPlatformDdl = driver.FindElement(By.Id(TradingPlatformDdlSelector));
            TradingPlatformDdlOptions = TryInitDdlOptions(TradingPlatformDdl, 10);
            //new SelectElement(TradingPlatformDdl);
        }
        public void InitLeverage(IWebDriver driver)
        {
            LeverageDdl = driver.FindElement(By.Id(LeverageDdlSelector));
            LeverageDdlOptions = TryInitDdlOptions(LeverageDdl, 10);
            //new SelectElement(LeverageDdl);
        }
        public void InitPlatformGroupOptions(IWebDriver driver)
        {
            BaseTest.waitElemntToDisapearById(driver, "tpLoader",10);
            BaseTest.waitElemntVisibleByClassName(driver, PlatformGroupDdlOptionsSelector, 8);

            PlatformGroupDdlOptions = TryInitDdlCollectionOptions(driver, PlatformGroupDdl, PlatformGroupDdlOptionsSelector, 10);
                //= driver.FindElements(By.ClassName(PlatformGroupDdlOptionsSelector));
        }       

        private SelectElement TryInitDdlOptions(IWebElement ddl, int seconds)
        {
            SelectElement ddlOptions = new SelectElement(ddl);
            if (BaseTest.IsTimerToWaitSeconds(seconds))
            {
                if (ddlOptions.Options.Count > 1)
                {
                    return ddlOptions;
                }
                else
                {
                    return TryInitDdlOptions(ddl, seconds);
                }
            }
            else
            {
                throw new Exception("TryInitDdlOptions timeout, " + seconds.ToString());
            }
        }

        private ReadOnlyCollection<IWebElement> TryInitDdlCollectionOptions(IWebDriver driver,IWebElement ddlToClick, string selector, int seconds)
        {
            ddlToClick.Click();
            ddlToClick.Click();
            ReadOnlyCollection<IWebElement> ddlOptions = driver.FindElements(By.ClassName(selector));
            if (BaseTest.IsTimerToWaitSeconds(seconds))
            {
                if (ddlOptions.Count > 1)
                {
                    return ddlOptions;
                }
                else
                {
                    return TryInitDdlCollectionOptions(driver, ddlToClick, selector, seconds);
                }
            }
            else
            {
                throw new Exception("TryInitDdlOptions timeout, " + seconds.ToString());
            }
        }

    }
}
