﻿using CRM_SELENIUM.BaseModule;
using OpenQA.Selenium;

namespace CRM_SELENIUM.Models.UserPageModels.TradingAccountsModels
{
    public class TradingAccountsPageModel
    {
        private readonly string CreateTradingAccountBtnSelector = "[href*='/CrmMain/CreateTradingAccount']"; 


        public IWebElement CreateTradingAccountBtn { get; set; }


        public TradingAccountsPageModel(IWebDriver driver)
        {
            BaseTest.WaitElemntToBeClickableByCssSelector(driver, CreateTradingAccountBtnSelector,8);
            CreateTradingAccountBtn = driver.FindElement(By.CssSelector(CreateTradingAccountBtnSelector));
        }
    }
}
