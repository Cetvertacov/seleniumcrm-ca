﻿using CRM_SELENIUM.BaseModule;
using OpenQA.Selenium;

namespace CRM_SELENIUM.Models.UserPageModels.UserPageTabs
{
   public  class ClientPageTabsModel
    {
        private readonly string ActionDdlSelector = "//*[@id='selectTab']/li[1]/a";
        private readonly string TradingAccountsSelector = "[href*='#Trading_account']";

        public IWebElement ActionDdl { get; set; }
        public IWebElement TradingAccounts { get; set; }

        public ClientPageActionsDdlOptionsModel ActionDdlOptions { get; set; }

        public ClientPageTabsModel(IWebDriver driver)
        {
            BaseTest.waitElemntVisibleByXPath(driver, ActionDdlSelector, 8);
            ActionDdl = driver.FindElement(By.XPath(ActionDdlSelector));
            TradingAccounts = driver.FindElement(By.CssSelector(TradingAccountsSelector));
        }

        public void InitClientPageActionDdlOptions(IWebDriver driver)
        {
            ActionDdlOptions = new ClientPageActionsDdlOptionsModel(driver);
        }
    }
}
