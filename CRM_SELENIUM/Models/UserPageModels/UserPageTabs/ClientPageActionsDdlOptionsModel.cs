﻿using CRM_SELENIUM.BaseModule;
using OpenQA.Selenium;

namespace CRM_SELENIUM.Models.UserPageModels.UserPageTabs
{
    public class ClientPageActionsDdlOptionsModel
    {
        private readonly string InternalTransferSelector = "[data-target*='#internTransferPartial']";
        private readonly string ArchiveClientSelector = "tryAddingToArchive";
        private readonly string PaymentOperationsSelector = "//*[@id='selectTab']/li[1]/ul/li[3]/a";
        private readonly string CommunicationSelector = "//*[@id='selectTab']/li[1]/ul/li[4]/a";

        private readonly string PromoteToIbSelector = "[href*='/AffiliateProgram/PromoteToIb']";
        private readonly string PromoteToEmployeeSelector = "[href*='/Employee/CreateEmployee']";

        private readonly string ResetUserPassSelector = "[data-target*='#resetPwd']";


        public IWebElement InternalTransfer { get; set; }
        public IWebElement ArchiveClient { get; set; }
        public IWebElement PaymentOperationsDdl { get; set; } //not clickable
        public IWebElement CommunicationDdl { get; set; } //not clickable
        public IWebElement PromoteToIB { get; set; }
        public IWebElement PromoteToEmployee { get; set; }
        public IWebElement ResetUserPass { get; set; }

        public ClientPagePayOprtDdlOptions PaymentOperationsDdlOptions {get;set;}


        public ClientPageActionsDdlOptionsModel(IWebDriver driver)
        {
            BaseTest.WaitElemntToBeClickableByCssSelector(driver, InternalTransferSelector, 8);

            InternalTransfer = driver.FindElement(By.CssSelector(InternalTransferSelector));
            ArchiveClient = driver.FindElement(By.Id(ArchiveClientSelector));
            PaymentOperationsDdl = driver.FindElement(By.XPath(PaymentOperationsSelector));
            CommunicationDdl = driver.FindElement(By.XPath(CommunicationSelector));
            ResetUserPass = driver.FindElement(By.CssSelector(ResetUserPassSelector));

          
        }
        public void InitPromoteToIB(IWebDriver driver)
        {
            PromoteToIB = driver.FindElement(By.CssSelector(PromoteToIbSelector));
        }

        public void InitPromoteToEmployee(IWebDriver driver)
        {
            PromoteToEmployee = driver.FindElement(By.CssSelector(PromoteToEmployeeSelector));
        }

        public void InitPaymentOperationsDdlOptions(IWebDriver driver)
        {
            PaymentOperationsDdlOptions = new ClientPagePayOprtDdlOptions(driver);
        }
    }
}
