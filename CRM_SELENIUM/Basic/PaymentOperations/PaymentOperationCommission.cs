﻿using CRM_SELENIUM.BaseModule;
using CRM_SELENIUM.Models;
using CRM_SELENIUM.Models.PaymentOperationModels.Commission;
using CRM_SELENIUM.Models.SearchesModels.ClientSearchModels;
using CRM_SELENIUM.Models.UserPageModels.UserPageTabs;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.Threading;

namespace CRM_SELENIUM.Basic
{
    public class PaymentOperationCommission
    {
        private readonly string MT4_LIVE = "MT4_LIVE";
        private readonly string COMMISSION_FORM_ID = "";
        private readonly string AMOUNT = "10";
        private IWebDriver driver { get; set; }
        private Actions WebActions { get; set; }
        private LogOnCRM logOn { get; set; }
        private SlideMenuModel slideMenu { get; set; }
        private ClientSearchFiltersModel filters { get; set; }
        private ClientSearchExtraFiltersModel extraFilters { get; set; }
        private ClientSearchDataTableModel dataTable { get; set; }
        private ClientPageTabsModel userTabs { get; set; }
        private CommissionModel commissionForm { get; set; }


        [SetUp]
        public void InitializeChrome()
        {
            driver = new ChromeDriver(BaseTest.LOCAL_PATH_TO_DRIVER);
            driver.Manage().Window.Maximize();
            WebActions = new Actions(driver);

        }

        [Test]
        public void Payment_operation_commission()
        {
            #region LogOn
            logOn = new LogOnCRM(driver);
            logOn.log_on_crm();
            #endregion

            #region SlideMenu
            slideMenu = new SlideMenuModel(driver);
            slideMenu.Crm.Click();
            slideMenu.initCRMDdl(driver);
            slideMenu.CrmDdlOptions.ClientSearch.Click();
            #endregion

            #region Filters
            extraFilters = new ClientSearchExtraFiltersModel(driver);
            extraFilters.CollapseExtend.Click();
            extraFilters.RoleDdlOptions.SelectByValue("IB");

            filters = new ClientSearchFiltersModel(driver);
            filters.IBLevel.From.Clear();
            filters.IBLevel.From.SendKeys("1");
            filters.SearchBtn.Click();
            #endregion

            TryIbIfHasClientBack();
            BaseTest.waitElemntToDisapearById(driver, COMMISSION_FORM_ID, 8); //if not disapear then test fail
        }

        [TearDown]
        public void EndTest()
        {
            foreach (var window in driver.WindowHandles)
            {
                driver.SwitchTo().Window(window);
                driver.Close();
            }
            BaseTest.killChromeProcess();
        }

        private void TryIbIfHasClientBack()
        {
            dataTable = new ClientSearchDataTableModel(driver);
            string baseWindowName = driver.CurrentWindowHandle;
            foreach (var ib in dataTable.IBIds)
            {
                WebActions.KeyDown(Keys.Control).MoveToElement(ib).Click().Perform();
               // List<String> tabs = new List<String>(driver.WindowHandles);               
                driver.SwitchTo().Window(driver.WindowHandles[1]);
                WebActions = new Actions(driver);

                IBPageActionTab();
                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles[1]);
                if (!FillCommissionForm())
                {
                    driver.Close();
                    driver.SwitchTo().Window(driver.WindowHandles[0]);
                    continue;
                }
                break;
            }
         
        }

        private void IBPageActionTab()
        {
            userTabs = new ClientPageTabsModel(driver);
           
            userTabs.ActionDdl.Click();
            userTabs.InitClientPageActionDdlOptions(driver);
            WebActions.MoveToElement(userTabs.ActionDdlOptions.PaymentOperationsDdl).Perform();
            userTabs.ActionDdlOptions.InitPaymentOperationsDdlOptions(driver); 
            userTabs.ActionDdlOptions.PaymentOperationsDdlOptions.Commission.Click();
            
        }
        private bool FillCommissionForm()
        {
            commissionForm = new CommissionModel(driver);
            if (commissionForm.TradingAccountDdlOptions.Options.Count > 2)
                return false;
            
            commissionForm.TradingAccountDdlOptions.SelectByIndex(1);
            TryFillAmount(commissionForm.Amount, 10);
            commissionForm.AssociatedClientDdl.Click();
            commissionForm.InitAssociatedClientDdlOptions(driver);

            if (commissionForm.AssociatedClientDdlOptions.Count < 2)
                return false;

            commissionForm.AssociatedClientDdlOptions[1].Click();
            commissionForm.InitSourceTradingAccount(driver);

            if (commissionForm.SourceTradingAccountDdlOptions.Options.Count < 2)
                return false;

            commissionForm.SourceTradingAccountDdlOptions.SelectByIndex(1);

            DateTime date = DateTime.UtcNow;
            commissionForm.Comment.SendKeys("Testing Selen" + date.ToString());
        
            commissionForm.SubmitBtn.Click();

            BaseTest.waitElemntToDisapearById(driver, "deposit-form-wrapper", 8);

            return true;
        }

        private void TryFillAmount(IWebElement amount, int seconds)
        {
           
            if (BaseTest.IsTimerToWaitSeconds(seconds))
            {

                if (amount.GetAttribute("value") != "10")
                {
                    amount.Clear();
                    amount.SendKeys("10");
                    TryFillAmount(amount,seconds);
                }
                else
                {
                    return;
                }
            }
            else
            {
                throw new Exception("TryFillAmount timeout, " + seconds.ToString());
            }
        }

    }
}
