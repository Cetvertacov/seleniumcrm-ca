﻿using CRM_SELENIUM.BaseModule;
using CRM_SELENIUM.Models;
using CRM_SELENIUM.Models.PaymentOperationModels.Compensation;
using CRM_SELENIUM.Models.SearchesModels.TradingAccountSearchModels;
using CRM_SELENIUM.Models.TAPageModels;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using System;

namespace CRM_SELENIUM.Basic
{
    public class PaymentOperationFee
    {
        private readonly string MT4_LIVE = "MT4_LIVE";
        private readonly string FEE_FORM_ID = "deposit-form-wrapper";
        private readonly string AMOUNT = "10";
        private IWebDriver driver { get; set; }
        private Actions WebActions { get; set; }
        private LogOnCRM logOn { get; set; }
        private SlideMenuModel slideMenu { get; set; }
        private TASearchFiltersModel filters { get; set; }
        private TASearchExtraFilters extraFilters { get; set; }
        private TASearchDataTableModel dataTable { get; set; }
        private TAPageTabsModel taActions { get; set; }
        private CompensationModel feeForm { get; set; }


        [SetUp]
        public void InitializeChrome()
        {
            driver = new ChromeDriver(BaseTest.LOCAL_PATH_TO_DRIVER);
            driver.Manage().Window.Maximize();
            WebActions = new Actions(driver);

        }

        [Test]
        public void Payment_operation_fee()
        {
            #region LogOn
            logOn = new LogOnCRM(driver);
            logOn.log_on_crm();
            #endregion

            #region SlideMenu
            slideMenu = new SlideMenuModel(driver);
            slideMenu.Crm.Click();
            slideMenu.initCRMDdl(driver);
            slideMenu.CrmDdlOptions.TradingAccountSearch.Click();
            #endregion

            #region Filters
            extraFilters = new TASearchExtraFilters(driver);
            extraFilters.CollapseExtend.Click();
            extraFilters.TradingPlatformDdlOptions.SelectByValue(MT4_LIVE);

            filters = new TASearchFiltersModel(driver);
            filters.Balance.From.SendKeys(AMOUNT);
            filters.SearchBtn.Click();
            #endregion

            #region DataTable
            dataTable = new TASearchDataTableModel(driver);
            dataTable.TAsNumbers[1].Click(); //click on second TA
            #endregion

            #region TAPageActions
            taActions = new TAPageTabsModel(driver);
            taActions.ActionDdl.Click();
            taActions.InitTAPageActionDdlOptions(driver);
            WebActions.MoveToElement(taActions.ActionDdlOptions.PaymentOperationsDdl).Perform();
            taActions.ActionDdlOptions.InitPaymentOperationsDdlOptions(driver);
            taActions.ActionDdlOptions.PaymentOperationsDdlOptions.Compensation.Click();
            #endregion

            #region FillCompensationForm
            feeForm = new CompensationModel(driver);
            feeForm.TradingAccountDdlOptions.SelectByIndex(1);
            feeForm.Amount.Clear();
            feeForm.Amount.SendKeys(AMOUNT);          
            var date = DateTime.UtcNow;
            feeForm.Comment.SendKeys("Testing Selen"+ date.ToString());
            feeForm.SubmitBtn.Click();

            BaseTest.waitElemntToDisapearById(driver, FEE_FORM_ID, 8); //if not disapear then test fail
            #endregion
        }

        [TearDown]
        public void EndTest()
        {
            driver.Close();
            BaseTest.killChromeProcess();
        }
    }
}
