﻿using CRM_SELENIUM.BaseModule;
using CRM_SELENIUM.Models;
using CRM_SELENIUM.Models.SearchesModels.ClientSearchModels;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;

namespace CRM_SELENIUM.Basic
{
    public class PromoteToIbLive
    {
        IWebDriver driver;
        private readonly string URL_PATH_TO_GET_1 = "Account/LogOn";
        private readonly string ROLE_LIVE = "Live";
        private readonly string MT4_LIVE = "MT4_LIVE";
        private readonly string SUCCESS_ALERTBOX_SELECTOR = "div:not([style*='display: none'])[class*='alert-success']";
        private readonly string NAME_IB_PROFILE = "Comission Profile Name";

        private LogOnCRM logOn;
        private SlideMenuModel slideMenu;
        private ClientSearchExtraFiltersModel clientExtraSearch;
        private ClientSearchFiltersModel clientPortlet;
        private ClientSearchDataTableModel clientSearchDataTable;
        private TabsUserInfoPageModel userInfoPage;
        private PromoteToIbModel promoteToIbPage;


        [SetUp]
        public void InitializeChrome()
        {
            driver = new ChromeDriver(BaseTest.LOCAL_PATH_TO_DRIVER);           
            driver.Manage().Window.Maximize();
        }

        [Test]
        public void Promote_to_ib_from_live()
        {
            {
                driver.Url = BaseTest.BASE_URL_CRM + URL_PATH_TO_GET_1;
                try
                {
                    #region LogIn
                    logOn = new LogOnCRM(driver);
                    logOn.log_on_crm();
                    #endregion
                }
                catch (Exception e)
                { throw new Exception("PromoteToIb - LogIn failed" + e); }
                try
                {
                    #region GoToClientSearch

                    slideMenu = new SlideMenuModel(driver);
                    slideMenu.Crm.Click(); //click CRM element from sldie menu    
                    slideMenu.initCRMDdl(driver);
                    slideMenu.CrmDdlOptions.ClientSearch.Click(); //click client search element from sub menu
                    #endregion
                }
                catch (Exception e)
                { throw new Exception("PromoteToIb - GoToClientSearch failed" + e); }

                try
                {
                    #region ClientSearchFilters                
                    clientExtraSearch = new ClientSearchExtraFiltersModel(driver);

                    clientExtraSearch.CollapseExtend.Click();
                    clientExtraSearch.RoleDdlOptions.SelectByText(ROLE_LIVE);

                    clientPortlet = new ClientSearchFiltersModel(driver);
                    clientPortlet.SearchBtn.Click();

                    clientSearchDataTable = new ClientSearchDataTableModel(driver);
                    clientSearchDataTable.ClientsIds[1].Click();

                    #endregion
                }
                catch (Exception e)
                { throw new Exception("PromoteToIb - ClientSearchFilters failed" + e); }
                try
                {
                    #region GoToPromotePage 
                    //TODO WRONG MODEL USAGE
                    userInfoPage = new TabsUserInfoPageModel(driver);
                    userInfoPage.ActionsDdl.Click();
                    userInfoPage.InitActionTabOptions(driver);
                    userInfoPage.ActionTabOptions.InitPromoteToIB(driver);
                    userInfoPage.ActionTabOptions.PromoteToIB.Click();
                    #endregion
                }
                catch (Exception e)
                { throw new Exception("PromoteToIb - GoToPromotePage failed" + e); }
                try
                {
                    #region FillPromoteForm
                    promoteToIbPage = new PromoteToIbModel(driver);
                    promoteToIbPage.TradingPlatformDllOptions.SelectByValue(MT4_LIVE);
                    BaseTest.WaitSpinnerDisapear(driver);

                    promoteToIbPage.TradingAccountDdlOptions.SelectByText("Create New Trading Account"); //TODO: create enum
                    promoteToIbPage.BtnDefaultTAMappingAdd.Click();
                    BaseTest.WaitSpinnerDisapear(driver);

                    promoteToIbPage.SetCommProfileMappingFieleds(driver);
                    promoteToIbPage.ProductDll.Click();
                    promoteToIbPage.ProductDllOptions[0].Click();

                    promoteToIbPage.TypeDdlOptions.SelectByText("Fixed Promo"); //TODO: create enum
                    BaseTest.WaitSpinnerDisapear(driver);
                    promoteToIbPage.TradingPlatformCommProfMapDdlOptions.SelectByIndex(1);
                    BaseTest.WaitSpinnerDisapear(driver);


                    promoteToIbPage.InitCommissionConfigDdl(driver);
                    promoteToIbPage.CommNamesConfigsDdl.Click();
                    promoteToIbPage.InitCommissionConfigOptions(driver);

                    promoteToIbPage.CommNamesConfigsDdlOptions[1].Click();
                    promoteToIbPage.DisplayName.SendKeys(NAME_IB_PROFILE);
                    promoteToIbPage.BtnDCommProfAddMaping.Click();

                    promoteToIbPage.SetIbNestingLevelFields(driver);
                    promoteToIbPage.NestingLevelDdlOptions.SelectByIndex(1);

                    promoteToIbPage.SubmitBtn.Click();

                    BaseTest.TryGetElementByCssSelector(driver, SUCCESS_ALERTBOX_SELECTOR, 8);
                    #endregion
                }
                catch (Exception e)
                { throw new Exception("PromoteToIb - FillPromoteForm failed" + e); }
            }
        }

        [TearDown]
        public void EndTest()
        {
            driver.Close();
            BaseTest.killChromeProcess();
        }
    }
}
